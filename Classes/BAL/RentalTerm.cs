﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultiLocationsManager.Classes.BAL
{
    internal class RentalTerm
    {
        #region "Internal Properties"

        public Int64 Id { get; private set; } = -1;
        public int Age { get; private set; }
        public int MaxMileage { get; private set; }
        public double OverMilagePrice { get; private set; }
        public int NumbersOfMonthlyPayments { get; private set; }

        #endregion


        #region "Constructor"

        internal RentalTerm() { }

        /// <summary>
        /// Instancie un Terms_Location à partir de la DB.
        /// </summary>
        /// <param name="sqlDataReader"></param>
        internal RentalTerm(SqlDataReader sqlDataReader)
        {
            this.Id = sqlDataReader.GetInt64(sqlDataReader.GetOrdinal("Terms_LocationID"));
            this.Age = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("NombreAnnées"));
            this.MaxMileage = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("KiloMax"));
            this.OverMilagePrice = (double) sqlDataReader.GetDecimal(sqlDataReader.GetOrdinal("SurPrimeKilomètrage"));
            this.NumbersOfMonthlyPayments = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("NombrePaimentMensuel"));
        }

        #endregion


        #region "Internal Functions/Setters"

        internal RentalTerm SetId(Int64 Id = -1)
        {
            this.Id = Id;
            return this;
        }


        internal RentalTerm SetAge(int Age)
        {
            this.Age = Age;
            return this;
        }


        internal RentalTerm SetMaxMileage(int maxMileage)
        {
            this.MaxMileage = maxMileage;
            return this;
        }

        
        internal RentalTerm SetOverMileagePrice(double overMileagePrice)
        {
            this.OverMilagePrice = overMileagePrice;
            return this;
        }


        internal RentalTerm SetNumbersOfMonthlyPayments(int numbersOfMonthlyPayments)
        {
            this.NumbersOfMonthlyPayments = numbersOfMonthlyPayments;
            return this;
        }

        #endregion
    }
}
