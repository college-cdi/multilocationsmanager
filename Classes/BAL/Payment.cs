﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultiLocationsManager.Classes.BAL
{
    internal class Payment
    {
        public Int64 Id { get; private set; }
        public DateTime Date { get; private set; }
        public double Amount { get; private set; }
        public Rental? Rental { get; private set; }
    }
}
