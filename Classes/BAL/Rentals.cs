﻿using MultiLocationsManager.Classes.DATA.SQL;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultiLocationsManager.Classes.BAL
{
    internal class Rentals
    {
        private List<Rental> vlstRental = new List<Rental>();

        public int Count { get { return this.vlstRental.Count; } }
        public IList<Rental> Items { get => this.vlstRental; }

        #region "Constructor"

        public Rentals()
        {
        }

        #endregion


        #region "Internal Database Functions"

        /// <summary>
        /// Charge les Locations dans la DB.
        /// </summary>
        internal void Load()
        {
            RentalDbFnc.Load(this);
        }


        /// <summary>
        /// Insère une nouvelle location dans la DB.
        /// </summary>
        /// <param name="rental">Référence de la Location à inséré.</param>
        internal void Insert(Rental rental)
        {
            RentalDbFnc.Insert(rental);
        }


        /// <summary>
        /// Supprime une Location dans la DB.
        /// </summary>
        /// <param name="rental">Référence de la Location.</param>
        internal bool Delete(Rental rental)
        {
            return RentalDbFnc.Delete(rental);
        }

        #endregion


        #region "Public Helper Function"

        /// <summary>
        /// Instancie une liste de Locations à partir de la DB.
        /// </summary>
        /// <param name="sqlDataReader">SQL DATA READER</param>
        public void Add(SqlDataReader sqlDataReader)
        {
            this.vlstRental.Add(new Rental(sqlDataReader));
        }


        public Rental Item(int index) {
            return this.vlstRental[index];
        }


        public Rental? Find(Rental rental)
        {
            return this.vlstRental.Find(_rental => _rental.Equals(rental));
        }

        public bool Contains(Rental rental)
        {
            return this.vlstRental.Contains(rental);
        }

        #endregion


    }
}
