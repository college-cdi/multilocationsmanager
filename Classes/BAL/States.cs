﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultiLocationsManager.Classes.BAL
{
    internal class States
    {
        public string Text { get; set; } = "";
        public string Value { get; set; } = "";

        public States(string text, string value)
        {
            Text = text;
            Value = value;
        }

        public override string ToString()
        {
            return this.Text;
        }
    }
}
