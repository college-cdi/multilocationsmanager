﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultiLocationsManager.Classes.BAL
{
    class CarOption
    {
        #region "Internal Properties"

        public Int64 Id { get; private set; }
        public string Name { get; private set; }

        #endregion


        #region "Constructor"

        internal CarOption() { }


        /// <summary>
        /// Instancie une nouvelle Option à partir de la DB.
        /// </summary>
        /// <param name="sqlDataReader"></param>
        internal CarOption(SqlDataReader sqlDataReader)
        {
            this.Id = sqlDataReader.GetInt64(sqlDataReader.GetOrdinal("OptionID"));
            this.Name = sqlDataReader.GetString(sqlDataReader.GetOrdinal("Option"));
        }

        #endregion
    }
}
