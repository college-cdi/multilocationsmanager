﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultiLocationsManager.Classes.BAL
{
    internal class CarModel
    {
        #region "Internal Properties"

        public Int64 Id { get; private set; }
        public string Name { get; private set; }

        #endregion


        #region "Constructor"

        internal CarModel() { }


        /// <summary>
        /// Instancie un nouveau Modèle à partir de la DB.
        /// </summary>
        /// <param name="sqlDataReader"></param>
        internal CarModel(SqlDataReader sqlDataReader) 
        {
            this.Id = sqlDataReader.GetInt64(sqlDataReader.GetOrdinal("ModèleID"));
            this.Name = sqlDataReader.GetString(sqlDataReader.GetOrdinal("Modèle"));
        }

        #endregion
    }
}
