﻿using MultiLocationsManager.Classes.DATA.SQL;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultiLocationsManager.Classes.BAL
{
    internal class RentalTerms
    {
        private List<RentalTerm> vlstRentalTerms = new List<RentalTerm>();

        public int Count { get { return this.vlstRentalTerms.Count; } }
        public IList<RentalTerm> Items { get => this.vlstRentalTerms; }

        #region "Constructor"

        public RentalTerms()
        {
        }

        #endregion


        #region "Internal Database Functions"

        /// <summary>
        /// Charge les Locations dans la DB.
        /// </summary>
        internal void Load()
        {
            RentalTermsDbFnc.Load(this);
        }



        /// <summary>
        /// Insère une nouvelle location dans la DB.
        /// </summary>
        /// <param name="rental">Référence de la Location à inséré.</param>
        internal void Insert(RentalTerm rentalTerm)
        {
            RentalTermsDbFnc.Insert(rentalTerm);
        }


        /// <summary>
        /// Supprime une Location dans la DB.
        /// </summary>
        /// <param name="rental">Référence de la Location.</param>
        internal bool Delete(RentalTerm rentalTerm)
        {
            return RentalTermsDbFnc.Delete(rentalTerm);
        }

        #endregion


        #region "Public Helper Function"

        /// <summary>
        /// Instancie une liste de Locations à partir de la DB.
        /// </summary>
        /// <param name="sqlDataReader">SQL DATA READER</param>
        public void Add(SqlDataReader sqlDataReader)
        {
            this.vlstRentalTerms.Add(new RentalTerm(sqlDataReader));
        }


        public RentalTerm Item(int index)
        {
            return this.vlstRentalTerms[index];
        }


        public RentalTerm? Find(RentalTerm rentalTerm)
        {
            return this.vlstRentalTerms.Find(_rentalTerm => _rentalTerm.Equals(rentalTerm));
        }

        public bool Contains(RentalTerm rentalTerm)
        {
            return this.vlstRentalTerms.Contains(rentalTerm);
        }

        #endregion


    }

}
