﻿using MultiLocationsManager.Classes.DATA.SQL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Navigation;

namespace MultiLocationsManager.Classes.BAL
{
    internal class Rental
    {
        #region "internal Properties"


        public Int64 Id { get; set; } = -1;
        public DateTime LocationStartDate { get; set; }
        public DateTime FirstPaymentDate { get; private set; }
        public double FirstPaymentAmount { get; private set; } = 0;
        public Int64 MileageAtStart { get; private set; } = 0;
        public Int64 MileageAtEnd { get; private set; } = 0;
        public Client? Client { get; private set; }
        public RentalTerm? Terms { get; private set; }
        public Vehicle? Vehicle { get; private set; }

        public bool isSelected { get; set; } = false;

        #endregion


        #region "Constructor"

        public Rental()
        {
        }


        /// <summary>
        /// Instancie une Location à partir de la DB.
        /// </summary>
        /// <param name="sqlDataReader"></param>
        public Rental(SqlDataReader sqlDataReader)
        {
            this.Id = sqlDataReader.GetInt64(sqlDataReader.GetOrdinal("LocationID"));
            this.LocationStartDate = sqlDataReader.GetDateTime(sqlDataReader.GetOrdinal("DateDébutLocation"));
            this.FirstPaymentDate = sqlDataReader.GetDateTime(sqlDataReader.GetOrdinal("DatePremierPaeiement"));
            this.FirstPaymentAmount = (double)sqlDataReader.GetDecimal(sqlDataReader.GetOrdinal("MontantPremierPaiement"));
            this.Client = new Client(sqlDataReader);
            this.Terms = new RentalTerm(sqlDataReader);
            this.Vehicle = new Vehicle(sqlDataReader);
            if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("KilométrageDébutLocation"))) this.MileageAtStart = sqlDataReader.GetInt64(sqlDataReader.GetOrdinal("KilométrageDébutLocation"));
            if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("KilométrageFinLocation"))) this.MileageAtEnd = sqlDataReader.GetInt64(sqlDataReader.GetOrdinal("KilométrageFinLocation"));
        }

        #endregion


        #region "Internal Setters/Function"

        internal Rental SetFirstPaymentAmount(string firstPayment)
        {
            bool Success = Double.TryParse(firstPayment, out double doubleFirstPayment);

            if (Success)
            {
                this.FirstPaymentAmount = doubleFirstPayment;
            } else
            {
                MessageBox.Show("La valeur entré n'est pas un nombre", "NaN", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            return this;
        }


        internal Rental SetLocationStartDate(DateTime locationStartDate)
        {
            this.LocationStartDate = locationStartDate;
            return this;
        }


        internal Rental SetFirstPaymentDate(DateTime firstPaymentDate)
        {
            this.FirstPaymentDate = firstPaymentDate;
            return this;
        }


        internal Rental SetVehicle(Vehicle vehicle)
        {
            this.Vehicle = vehicle;
            return this;
        }


        internal Rental SetTerms(RentalTerm rentalTerm)
        {
            this.Terms = rentalTerm;
            return this;
        }


        internal Rental SetMileageAtStart(string mileageAtStart)
        {
            bool Success = int.TryParse(mileageAtStart, out int intMileageAtStart);
            if (Success)
            {
                this.MileageAtStart = intMileageAtStart;
            } else
            {
                MessageBox.Show("La valeur entré n'est pas un nombre", "NaN", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            return this;
        }

        internal Rental SetMileageAtEnd(string mileageAtEnd)
        {
            bool Success = int.TryParse(mileageAtEnd, out int intMileageAtEnd);
            if (Success)
            {
                this.MileageAtEnd = intMileageAtEnd;
            }
            else
            {
                MessageBox.Show("La valeur entré n'est pas un nombre", "NaN", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            return this;
        }


        internal Rental SetClient(Client client)
        {
            this.Client = client;
            return this;
        }

        #endregion


        #region "Internal DBFunctions"

        internal bool Edit()
        {
            return RentalDbFnc.Edit(this);
        }


        internal bool Insert()
        {
            return RentalDbFnc.Insert(this);
        }

        #endregion

    }
}
