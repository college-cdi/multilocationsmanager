﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultiLocationsManager.Classes.BAL
{
    internal class Vehicle
    {
        #region "Internal Properties"

        public string Niv { get; private set; }
        public CarModel Model { get; private set; }
        public CarType Type { get; private set; }
        public CarColor Color { get; private set; }
        public CarOption Option { get; private set; }
        public CarTransmission Transmission { get; private set; }
        public int Year { get; private set; }
        public double Value { get; private set; }

        public string _Model { get => $"{this.Model.Name} {this.Color.Name}"; }

        #endregion


        #region "Constructor"

        internal Vehicle() { }

        /// <summary>
        /// Instancie un Véhicule à partir de la DB.
        /// </summary>
        /// <param name="sqlDataReader"></param>
        internal Vehicle(SqlDataReader sqlDataReader)
        {
            this.Niv = sqlDataReader.GetString(sqlDataReader.GetOrdinal("VéhiculesNIV"));
            this.Year = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("Année"));
            this.Value = (double) sqlDataReader.GetDecimal(sqlDataReader.GetOrdinal("Valeur"));
            this.Model = new CarModel(sqlDataReader);
            this.Type = new CarType(sqlDataReader);
            this.Color = new CarColor(sqlDataReader);
            this.Option = new CarOption(sqlDataReader);
            this.Transmission = new CarTransmission(sqlDataReader);
    }

        #endregion
    }
}
