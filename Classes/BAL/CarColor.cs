﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultiLocationsManager.Classes.BAL
{
    internal class CarColor
    {
        #region "Internal Properties"

        public Int64 Id { get; private set; }
        public string Name { get; private set; }

        #endregion


        #region "Constructor"

        internal CarColor() { }


        /// <summary>
        /// Instancie une nouvelle Couleur à partir de la DB.
        /// </summary>
        /// <param name="sqlDataReader"></param>
        internal CarColor(SqlDataReader sqlDataReader)
        {
            this.Id = sqlDataReader.GetInt64(sqlDataReader.GetOrdinal("CouleurID"));
            this.Name = sqlDataReader.GetString(sqlDataReader.GetOrdinal("Couleur"));
        }

        #endregion
    }
}
