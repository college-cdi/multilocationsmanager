﻿using MultiLocationsManager.Classes.DATA.SQL;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultiLocationsManager.Classes.BAL
{
    internal class Clients
    {
        private List<Client> vlstClients = new List<Client>();

        public int Count { get { return this.vlstClients.Count; } }
        public IList<Client> Items { get => this.vlstClients; }

        #region "Constructor"

        public Clients()
        {
        }

        #endregion


        #region "Internal Database Functions"

        /// <summary>
        /// Charge les Locations dans la DB.
        /// </summary>
        internal void Load()
        {
            ClientsDbFnc.Load(this);
        }



        /// <summary>
        /// Insère une nouvelle location dans la DB.
        /// </summary>
        /// <param name="rental">Référence de la Location à inséré.</param>
        internal void Insert(Client Client)
        {
            ClientsDbFnc.Insert(Client);
        }


        /// <summary>
        /// Supprime une Location dans la DB.
        /// </summary>
        /// <param name="rental">Référence de la Location.</param>
        internal bool Delete(Client Client)
        {
            return ClientsDbFnc.Delete(Client);
        }

        #endregion


        #region "Public Helper Function"

        /// <summary>
        /// Instancie une liste de Locations à partir de la DB.
        /// </summary>
        /// <param name="sqlDataReader">SQL DATA READER</param>
        public void Add(SqlDataReader sqlDataReader)
        {
            this.vlstClients.Add(new Client(sqlDataReader));
        }


        public Client Item(int index)
        {
            return this.vlstClients[index];
        }


        public Client? Find(Client Client)
        {
            return this.vlstClients.Find(_Client => _Client.Equals(Client));
        }

        public bool Contains(Client Client)
        {
            return this.vlstClients.Contains(Client);
        }

        #endregion


    }
}
