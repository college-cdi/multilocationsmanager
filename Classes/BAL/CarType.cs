﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultiLocationsManager.Classes
{
    internal class CarType
    {
        #region "Internal Properties"

        public Int64 Id { get; private set; }
        public string Name { get; private set; }

        #endregion

        #region "Constructor"

        internal CarType() { }


        /// <summary>
        /// Instancie un nouveau Type à partir de la DB.
        /// </summary>
        /// <param name="sqlDataReader"></param>
        internal CarType(SqlDataReader sqlDataReader)
        {
            this.Id = sqlDataReader.GetInt64(sqlDataReader.GetOrdinal("TypeID"));
            this.Name = sqlDataReader.GetString(sqlDataReader.GetOrdinal("Type"));
        }

        #endregion
    }
}
