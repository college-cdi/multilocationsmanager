﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using BCrypt.Net;
using MultiLocationsManager.Classes.Data;

namespace MultiLocationsManager.Classes.BAL
{
    internal class ActiveUser
    {

        #region "Internal Properties"

        public string UserName { get; private set; }
        public string Prenom { get; private set; }
        public string Nom { get; private set; }
        public string Password { get; private set; }
        public string HashedPassword { get; private set; }
        public bool isLoggedIn { get; private set; } = false;

        #endregion

        #region "Constructor/Destructor"

        internal ActiveUser(SqlDataReader sqlDataReader)
        {
            this.UserName = sqlDataReader["IdUtilisateur"].ToString();
            this.Prenom = sqlDataReader["Prenom"].ToString();
            this.Nom = sqlDataReader["Nom"].ToString();
        }

        /// <summary>
        /// Constructeur à vide pour initialiser l'utilisateur.
        /// </summary>
        internal ActiveUser()
        {
        }
        #endregion

        #region "Internal Functions"

        /// <summary>
        /// Initialise l'utilisateur.
        /// </summary>
        /// <param name="sqlReader">SqlDataReader: Permet de récupérer les champs dans la base de donnée.</param>
        internal void Initialize(SqlDataReader sqlReader)
        {
            this.UserName = sqlReader["UserName"].ToString();
            this.Prenom = sqlReader["Prenom"].ToString();
            this.Nom = sqlReader["Nom"].ToString();
            this.isLoggedIn = true;
        }


        /// <summary>
        /// Initialise les données de l'utilisateur pour l'authentification.
        /// </summary>
        /// <param name="userName">Nom d'utilisateur.</param>
        /// <param name="password">Mot de passe de l'utilisateur.</param>
        internal void SetCredentials(string userName, string password)
        {
            this.UserName = userName;
            this.Password = password;
        }


        /// <summary>
        /// Vérifie que le mot de passe entré lors de l'authentification correspond au hash de la db.
        /// </summary>
        /// <param name="hashPassword">Mot de passe hashé storé dans la db.</param>
        internal bool verifyPassword(string hashPassword)
        {
            string tempPassword = this.Password;
            this.Password = "";
            this.HashedPassword = "";
            return BCrypt.Net.BCrypt.Verify(tempPassword, hashPassword);
        }


        internal void Encrypt()
        {
            this.HashedPassword = BCrypt.Net.BCrypt.HashPassword(this.Password);
        }


        internal ActiveUser SetFirstName(string firstName)
        {
            this.Prenom = firstName;
            return this;
        }



        internal ActiveUser SetUserName(string userName) 
        {
            this.UserName = userName;
            return this;
        }


        internal ActiveUser SetLastName(string lastName) 
        {
            this.Nom = lastName;
            return this;
        }

        internal ActiveUser SetPassword(string password) 
        {
            this.Password = password;
            return this;
        }

        internal bool Register()
        {
            return AuthenticationDbFnc.Insert(this);
        }

        #endregion
    }
}

