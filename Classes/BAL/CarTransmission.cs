﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultiLocationsManager.Classes
{
    internal class CarTransmission
    {
        #region "Internal Properties"

        public Int64 Id { get; private set; }
        public string Name { get; private set; }

        #endregion

        #region "Constructor"

        internal CarTransmission() { }


        /// <summary>
        /// Instancie une nouvelle Transmission à partir de la DB.
        /// </summary>
        /// <param name="sqlDataReader"></param>
        internal CarTransmission(SqlDataReader sqlDataReader)
        {
            this.Id = sqlDataReader.GetInt64(sqlDataReader.GetOrdinal("TransmissionID"));
            this.Name = sqlDataReader.GetString(sqlDataReader.GetOrdinal("Transmission"));
        }

        #endregion

    }
}
