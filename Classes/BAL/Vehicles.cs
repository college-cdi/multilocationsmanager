﻿using MultiLocationsManager.Classes.DATA.SQL;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultiLocationsManager.Classes.BAL
{
    internal class Vehicles
    {
        //@TODO
        private List<Vehicle> vlstVehicles = new List<Vehicle>();

        public int Count { get { return this.vlstVehicles.Count; } }
        public IList<Vehicle> Items { get => this.vlstVehicles; }

        #region "Constructor"

        public Vehicles()
        {
        }

        #endregion


        #region "Internal Database Functions"

        /// <summary>
        /// Charge les Locations dans la DB.
        /// </summary>
        internal void Load()
        {
            VehicleDbFnc.Load(this);
        }



        /// <summary>
        /// Insère une nouvelle location dans la DB.
        /// </summary>
        /// <param name="rental">Référence de la Location à inséré.</param>
        internal void Insert(Rental rental)
        {
            RentalDbFnc.Insert(rental);
        }


        /// <summary>
        /// Supprime une Location dans la DB.
        /// </summary>
        /// <param name="rental">Référence de la Location.</param>
        internal bool Delete(Rental rental)
        {
            return RentalDbFnc.Delete(rental);
        }

        #endregion


        #region "Public Helper Function"

        /// <summary>
        /// Instancie une liste de Locations à partir de la DB.
        /// </summary>
        /// <param name="sqlDataReader">SQL DATA READER</param>
        public void Add(SqlDataReader sqlDataReader)
        {
            this.vlstVehicles.Add(new Vehicle(sqlDataReader));
        }


        public Vehicle Item(int index)
        {
            return this.vlstVehicles[index];
        }


        public Vehicle? Find(Vehicle vehicle)
        {
            return this.vlstVehicles.Find(_rental => _rental.Equals(vehicle));
        }

        public bool Contains(Vehicle vehicle)
        {
            return this.vlstVehicles.Contains(vehicle);
        }

        #endregion


    }
}
