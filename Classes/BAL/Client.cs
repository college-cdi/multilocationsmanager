﻿using MultiLocationsManager.Classes.DATA.SQL;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultiLocationsManager.Classes.BAL
{
    internal class Client
    {
        #region "internal Properties"

        public Int64 ID { get; private set; } = -1;
        public string FirstName { get; private set; } = "";
        public string LastName { get; private set; } = "";
        public string Address { get; private set; } = "";
        public string State { get; private set; } = "";
        public string PostalCode { get; private set; } = "";
        public string PhoneNumber { get; private set; } = "";
        public string City { get; private set; } = "";

        public string FullName { get { return $"{this.FirstName} {this.LastName}"; } }
        public bool isSelected { get; set; } = false;

        #endregion


        #region "Constructor"

        /// <summary>
        /// Constructeur à vide.
        /// </summary>
        internal Client()
        {
        }

        /// <summary>
        /// Instancie un nouveau Client à partir d'un enregistrement de la DB.
        /// </summary>
        /// <param name="sqlDataReader">SQL Reader.</param>
        internal Client(SqlDataReader sqlDataReader)
        {
            this.ID = sqlDataReader.GetInt64(sqlDataReader.GetOrdinal("ClientID"));
            this.FirstName = sqlDataReader.GetString(sqlDataReader.GetOrdinal("Prénom"));
            this.LastName = sqlDataReader.GetString(sqlDataReader.GetOrdinal("Nom"));
            this.Address = sqlDataReader.GetString(sqlDataReader.GetOrdinal("Address"));
            this.State = sqlDataReader.GetString(sqlDataReader.GetOrdinal("Province"));
            this.PostalCode = sqlDataReader.GetString(sqlDataReader.GetOrdinal("CodePostal"));
            this.PhoneNumber = sqlDataReader.GetString(sqlDataReader.GetOrdinal("NuméroTéléphone"));
            this.City = sqlDataReader.GetString(sqlDataReader.GetOrdinal("Ville"));
        }

        #endregion


        #region "Internal Setters/Functions"

        internal Client SetFirstName(string firstName)
        {
            this.FirstName = firstName;
            return this;
        }


        internal Client SetLastName(string lastName)
        {
            this.LastName = lastName;
            return this;
        }


        internal Client SetPhoneNumber(string phoneNumber)
        {
            this.PhoneNumber = phoneNumber;
            return this;
        }


        internal Client SetPostalCode(string postalCode)
        {
            this.PostalCode = postalCode;
            return this;
        }


        internal Client SetCity(string city)
        {
            this.City = city;
            return this;
        }
        
        
        internal Client SetState(string state)
        {
            this.State = state;
            return this;
        }
        
        
        internal Client SetAddress(string address)
        {
            this.Address = address;
            return this;
        }

        #endregion


        #region "Internal DBFunctions"

        internal bool Edit()
        {
            return ClientsDbFnc.Edit(this);
        }


        internal bool Insert()
        {
            return ClientsDbFnc.Insert(this);
        }

        #endregion

    }
}
