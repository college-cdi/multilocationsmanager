﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using MultiLocationsManager.Classes.BAL;
using System.Diagnostics;

namespace MultiLocationsManager.Classes.Data
{
    internal static class AuthenticationDbFnc
    {
        private static SqlConnection Connection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
        internal static bool isOpen = false;

        /// <summary>
        /// Fonction pour authentifier un utilisateur.
        /// </summary>
        /// <param name="user">Utilisateur s'authentifiant.</param>
        internal static bool Authenticate(ActiveUser user)
        {
            try
            {
                SqlCommand command = new SqlCommand($"SELECT [Id]," +
                                                    $"    [UserName]," +
                                                    $"    [Password]," +
                                                    $"    [Prenom]," +
                                                    $"    [Nom]" +
                                                    $"    FROM [Utilisateurs]" +
                                                    $"    Where [UserName] = @UserName;", AuthenticationDbFnc.Connection);

                command.Parameters.Add("@UserName", System.Data.SqlDbType.VarChar);
                command.Parameters["@UserName"].Value = user.UserName;
                
                AuthenticationDbFnc.isOpen = true;
                AuthenticationDbFnc.Connection.Open();
                SqlDataReader sqlReader = command.ExecuteReader();
                if (sqlReader.HasRows)
                {
                    while (sqlReader.Read())
                    {
                        if (user.verifyPassword(sqlReader.GetString(sqlReader.GetOrdinal("Password" )) ))
                        {
                            user.Initialize(sqlReader);
                            return true;
                        } else
                        {
                            return false;
                        }
                    }
                }
            } catch (Exception ex)
            {
                MessageBox.Show("Impossible d'authentifier l'utilisateur.", "Une erreur est survenue...", MessageBoxButton.OK, MessageBoxImage.Error);
            } finally
            {
                AuthenticationDbFnc.isOpen = false;
                AuthenticationDbFnc.Connection.Close() ;
            }
            return false;
        }

        internal static bool Load()
        {
            try
            {
                SqlCommand command = new SqlCommand($"SELECT [Id]," +
                                                    $"    [UserName]," +
                                                    $"    [Password]," +
                                                    $"    [Prenom]," +
                                                    $"    [Nom]" +
                                                    $"    FROM [Utilisateurs];", AuthenticationDbFnc.Connection);

                AuthenticationDbFnc.isOpen = true;
                AuthenticationDbFnc.Connection.Open();
                SqlDataReader sqlReader = command.ExecuteReader();
                if (sqlReader.HasRows) return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                AuthenticationDbFnc.isOpen = false;
                AuthenticationDbFnc.Connection.Close();
            }
            return false;
        }


        internal static bool Insert(ActiveUser user)
        {
            try
            {
                SqlCommand command = new SqlCommand($"INSERT INTO [dbo].[Utilisateurs] (" +
                                                    $"    [UserName]" +
                                                    $"    ,[Password]" +
                                                    $"    ,[Prenom]" +
                                                    $"    ,[Nom]" +
                                                    $"  ) VALUES ( " +
                                                    $"     @UserName " +
                                                    $"    ,@Password " +
                                                    $"    ,@Prenom" +
                                                    $"    ,@Nom );", AuthenticationDbFnc.Connection);

                command.Parameters.Add("@UserName", System.Data.SqlDbType.VarChar);
                command.Parameters.Add("@Prenom", System.Data.SqlDbType.VarChar);
                command.Parameters.Add("@Nom", System.Data.SqlDbType.VarChar);
                command.Parameters.Add("@Password", System.Data.SqlDbType.VarChar);

                command.Parameters["@UserName"].Value = user.UserName;
                command.Parameters["@Prenom"].Value = user.Prenom;
                command.Parameters["@Nom"].Value = user.Nom;
                command.Parameters["@Password"].Value = user.HashedPassword;

                AuthenticationDbFnc.isOpen = true;
                AuthenticationDbFnc.Connection.Open();

                command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Impossible de créer l'utilisateur", "Une erreur est survenue...", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }
            finally
            {
                AuthenticationDbFnc.isOpen = false;
                AuthenticationDbFnc.Connection.Close();
            }
        }

    }
}
