﻿using MultiLocationsManager.Classes.Data;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using MultiLocationsManager.Classes.BAL;
using System.Diagnostics;
using System.Xml;

namespace MultiLocationsManager.Classes.DATA.SQL
{
    internal static class RentalTermsDbFnc
    {
        private static SqlConnection Connection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
        internal static bool isOpen = false;

        internal static void Load(RentalTerms RentalTerms)
        {
            try
            {
                SqlCommand command = new SqlCommand($"SELECT [TL].[ID] AS [Terms_LocationID]" +
                                                    $"    ,[TL].[KiloMax]" +
                                                    $"    ,[TL].[NombreAnnées]" +
                                                    $"    ,[TL].[NombrePaimentMensuel]" +
                                                    $"    ,[TL].[SurPrimeKilomètrage]" +
                                                    $"  FROM [MultiLocations6639].[dbo].[Termes_Locations] AS TL;", RentalTermsDbFnc.Connection);

                RentalTermsDbFnc.isOpen = true;
                RentalTermsDbFnc.Connection.Open();
                SqlDataReader sqlReader = command.ExecuteReader();
                if (sqlReader.HasRows)
                {
                    while (sqlReader.Read())
                    {
                        RentalTerms.Add(sqlReader);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                RentalTermsDbFnc.isOpen = false;
                RentalTermsDbFnc.Connection.Close();
            }
        }

        internal static bool Delete(RentalTerm rentalTerm)
        {
            try
            {
                SqlCommand command = new SqlCommand($"DELETE " +
                                                    $"  FROM [MultiLocations6639].[dbo].[LocationTerms]" +
                                                    $"    WHERE [Id]=@ID;", RentalTermsDbFnc.Connection);

                command.Parameters.Add("@ID", System.Data.SqlDbType.Int);
                command.Parameters["@ID"].Value = rentalTerm.Id;
                RentalTermsDbFnc.isOpen = true;
                RentalTermsDbFnc.Connection.Open();
                command.ExecuteNonQuery();
            } 
            catch (Exception ex)
            {
                Debug.Print(ex.Message);
                MessageBox.Show("Impossible de supprimer le terme de location.", "Une erreur est survenue...", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }
            finally
            {

            }
            return true;
        }


        internal static void Insert(RentalTerm rentalTerm)
        {
            throw new NotImplementedException();
        }


        internal static void InsertWithIDOutput(RentalTerm rentalTerm)
        {
            try
            {
                SqlCommand command = new SqlCommand($"INSERT INTO [MultiLocations6639].[dbo].[Termes_Locations] (" +
                                                    $"     [NombreAnnées]" +
                                                    $"    ,[KiloMax]" +
                                                    $"    ,[SurPrimeKilomètrage]" +
                                                    $"    ,[NombrePaimentMensuel]" +
                                                    $"  ) output INSERTED.ID VALUES (" +
                                                    $"     @NombreAnnées" +
                                                    $"    ,@KiloMax" +
                                                    $"    ,@SurPrimeKilometrage" +
                                                    $"    ,@NombrePaimentMensuel );", RentalTermsDbFnc.Connection);

                command.Parameters.Add("@NombreAnnées", System.Data.SqlDbType.Int);
                command.Parameters.Add("@KiloMax", System.Data.SqlDbType.Int);
                command.Parameters.Add("@SurPrimeKilometrage", System.Data.SqlDbType.Money);
                command.Parameters.Add("@NombrePaimentMensuel", System.Data.SqlDbType.Int);

                command.Parameters["@NombreAnnées"].Value = rentalTerm.Age;
                command.Parameters["@KiloMax"].Value = rentalTerm.MaxMileage;
                command.Parameters["@SurPrimeKilometrage"].Value = rentalTerm.OverMilagePrice;
                command.Parameters["@NombrePaimentMensuel"].Value = rentalTerm.NumbersOfMonthlyPayments;

                Debug.Print(command.CommandText);
                RentalTermsDbFnc.Connection.Open();
                Int64 rentalTermsID = (Int64) command.ExecuteScalar();
                rentalTerm.SetId(rentalTermsID);
            }
            catch (Exception ex) 
            {
                Debug.Print(ex.Message);
                MessageBox.Show("Impossible de créer le terme de location.", "Une erreur est survenue...", MessageBoxButton.OK, MessageBoxImage.Error);
                throw;
            }
            finally
            {
                RentalTermsDbFnc.Connection.Close();
            }
        }
    }
}
