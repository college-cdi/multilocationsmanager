﻿using MultiLocationsManager.Classes.Data;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using MultiLocationsManager.Classes.BAL;
using System.Diagnostics;

namespace MultiLocationsManager.Classes.DATA.SQL
{
    internal static class VehicleDbFnc
    {
        private static SqlConnection Connection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
        internal static bool isOpen = false;

        internal static void Load(Vehicles Vehicles)
        {
            try
            {
                SqlCommand command = new SqlCommand($"SELECT  [V].[NIV] AS [VéhiculesNIV]" +
                                                    $"    ,[V].[Année]" +
                                                    $"    ,[V].[Valeur]" +
                                                    $"    ,[Co].[ID] AS [CouleurID]" +
                                                    $"    ,[Co].[Couleur]" +
                                                    $"    ,[T].[ID] AS [TypeID]" +
                                                    $"    ,[T].[Type]" +
                                                    $"    ,[M].[ID] AS [ModèleID]" +
                                                    $"    ,[M].[Modèle]" +
                                                    $"    ,[Tr].[ID] AS [TransmissionID]" +
                                                    $"    ,[Tr].[Transmission]" +
                                                    $"    ,[O].[ID] AS [OptionID]" +
                                                    $"    ,[O].[Option]" +
                                                    $"  FROM [MultiLocations6639].[dbo].[Véhicules] AS [V]" +
                                                    $"    Inner Join [Couleurs] AS [Co] on [V].Couleur = [Co].ID" +
                                                    $"    Inner Join [Types] AS [T] ON [T].[ID] = [V].[Type]" +
                                                    $"    Inner Join [Modèles] AS [M] ON [M].[ID] = [V].[Modèle]" +
                                                    $"    Inner Join [Transmissions] AS [Tr] ON [Tr].[ID] = [V].[Transmission]" +
                                                    $"    Inner Join [Options] AS [O] ON [O].[ID] = [V].[Options];", VehicleDbFnc.Connection);

                VehicleDbFnc.isOpen = true;
                VehicleDbFnc.Connection.Open();
                SqlDataReader sqlReader = command.ExecuteReader();
                if (sqlReader.HasRows)
                {
                    while (sqlReader.Read())
                    {
                        Vehicles.Add(sqlReader);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                VehicleDbFnc.isOpen = false;
                VehicleDbFnc.Connection.Close();
            }
        }

        internal static bool Delete(Vehicle Vehicle)
        {
            try
            {
                SqlCommand command = new SqlCommand($"DELETE " +
                                                    $"  FROM [MultiLocations6639].[dbo].[Véhicules]" +
                                                    $"    WHERE [NIV]=@NIV;", VehicleDbFnc.Connection);

                command.Parameters.Add("@NIV", System.Data.SqlDbType.Int);
                command.Parameters["@NIV"].Value = Vehicle.Niv;
                VehicleDbFnc.isOpen = true;
                VehicleDbFnc.Connection.Open();
                command.ExecuteNonQuery();
            } 
            catch (Exception ex)
            {
                Debug.Print(ex.Message);
                MessageBox.Show("Impossible de supprimer la location.", "Une erreur est survenue...", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }
            finally
            {
                VehicleDbFnc.Connection.Close();
            }
            return true;
        }

        internal static void Insert(Rental rental)
        {
            throw new NotImplementedException();
        }
    }
}
