﻿using MultiLocationsManager.Classes.Data;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using MultiLocationsManager.Classes.BAL;
using System.Diagnostics;

namespace MultiLocationsManager.Classes.DATA.SQL
{
    internal static class ClientsDbFnc
    {
        private static SqlConnection Connection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
        internal static bool isOpen = false;

        internal static void Load(Clients Clients)
        {
            try
            {
                SqlCommand command = new SqlCommand($"SELECT [C].[ID] AS [ClientID]" +
                                                    $"    ,[C].[NuméroTéléphone]" +
                                                    $"    ,[C].[Prénom]" +
                                                    $"    ,[C].[Nom]" +
                                                    $"    ,[C].[Address]" +
                                                    $"    ,[C].[CodePostal]" +
                                                    $"    ,[C].[Ville]" +
                                                    $"    ,[C].[Province]" +
                                                    $"  FROM [MultiLocations6639].[dbo].[Clients] AS C;", ClientsDbFnc.Connection);

                ClientsDbFnc.isOpen = true;
                ClientsDbFnc.Connection.Open();
                SqlDataReader sqlReader = command.ExecuteReader();
                if (sqlReader.HasRows)
                {
                    while (sqlReader.Read())
                    {
                        Clients.Add(sqlReader);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                ClientsDbFnc.isOpen = false;
                ClientsDbFnc.Connection.Close();
            }
        }

        internal static bool Delete(Client Client)
        {
            try
            {
                SqlCommand command = new SqlCommand($"DELETE " +
                                                    $"  FROM [MultiLocations6639].[dbo].[Clients]" +
                                                    $"    WHERE [Id]=@ID;", ClientsDbFnc.Connection);

                command.Parameters.Add("@ID", System.Data.SqlDbType.Int);
                command.Parameters["@ID"].Value = Client.ID;
                ClientsDbFnc.isOpen = true;
                ClientsDbFnc.Connection.Open();
                command.ExecuteNonQuery();
            } 
            catch (Exception ex)
            {
                Debug.Print(ex.Message);
                MessageBox.Show("Impossible de supprimer la location.", "Une erreur est survenue...", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }
            finally
            {

            }
            return true;
        }

        internal static bool Insert(Client Client)
        {
            try
            {
                SqlCommand command = new SqlCommand($"INSERT INTO [MultiLocations6639].[dbo].[Clients] ( " +
                                                    $"     [NuméroTéléphone]" +
                                                    $"    ,[Prénom]" +
                                                    $"    ,[Nom]" +
                                                    $"    ,[Address]" +
                                                    $"    ,[CodePostal]" +
                                                    $"    ,[Ville]" +
                                                    $"    ,[Province]" +
                                                    $"  ) VALUES (" +
                                                    $"    @NuméroTéléphone" +
                                                    $"   ,@Prénom" +
                                                    $"   ,@Nom" +
                                                    $"   ,@Address" +
                                                    $"   ,@CodePostal" +
                                                    $"   ,@Ville" +
                                                    $"   ,@Province );", ClientsDbFnc.Connection);

                command.Parameters.Add("@NuméroTéléphone", System.Data.SqlDbType.VarChar);
                command.Parameters.Add("@Prénom", System.Data.SqlDbType.VarChar);
                command.Parameters.Add("@Nom", System.Data.SqlDbType.VarChar);
                command.Parameters.Add("@Address", System.Data.SqlDbType.VarChar);
                command.Parameters.Add("@CodePostal", System.Data.SqlDbType.VarChar);
                command.Parameters.Add("@Ville", System.Data.SqlDbType.VarChar);
                command.Parameters.Add("@Province", System.Data.SqlDbType.VarChar);

                command.Parameters["@NuméroTéléphone"].Value = Client.PhoneNumber;
                command.Parameters["@Prénom"].Value = Client.FirstName;
                command.Parameters["@Nom"].Value = Client.LastName;
                command.Parameters["@Address"].Value = Client.Address;
                command.Parameters["@CodePostal"].Value = Client.PostalCode;
                command.Parameters["@Ville"].Value = Client.City;
                command.Parameters["@Province"].Value = Client.State;

                ClientsDbFnc.isOpen = true;
                ClientsDbFnc.Connection.Open();
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Debug.Print(ex.Message);
                MessageBox.Show("Impossible de créer un nouveau client.", "Une erreur est survenue...", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }
            finally
            {
                ClientsDbFnc.isOpen = false;
                ClientsDbFnc.Connection.Close();
            }
            return true;
        }

        internal static bool Edit(Client client)
        {
            try
            {

                SqlCommand command = new SqlCommand($"UPDATE [MultiLocations6639].[dbo].[Clients] SET" +
                                                    $"  [NuméroTéléphone] = @NuméroTéléphone" +
                                                    $"  ,[Prénom] = @Prénom" +
                                                    $"  ,[Nom] = @Nom" +
                                                    $"  ,[Address] = @Address" +
                                                    $"  ,[CodePostal] = @CodePostal" +
                                                    $"  ,[Ville] = @Ville" +
                                                    $"  ,[Province] = @Province" +
                                                    $"    WHERE [ID] = @ID;", ClientsDbFnc.Connection);

                command.Parameters.Add("@ID", System.Data.SqlDbType.BigInt);
                command.Parameters.Add("@NuméroTéléphone", System.Data.SqlDbType.VarChar);
                command.Parameters.Add("@Prénom", System.Data.SqlDbType.VarChar);
                command.Parameters.Add("@Nom", System.Data.SqlDbType.VarChar);
                command.Parameters.Add("@Address", System.Data.SqlDbType.VarChar);
                command.Parameters.Add("@CodePostal", System.Data.SqlDbType.VarChar);
                command.Parameters.Add("@Ville", System.Data.SqlDbType.VarChar);
                command.Parameters.Add("@Province", System.Data.SqlDbType.VarChar);

                command.Parameters["@ID"].Value = client.ID;
                command.Parameters["@NuméroTéléphone"].Value = client.PhoneNumber;
                command.Parameters["@Prénom"].Value = client.FirstName;
                command.Parameters["@Nom"].Value = client.LastName;
                command.Parameters["@Address"].Value = client.Address;
                command.Parameters["@CodePostal"].Value = client.PostalCode;
                command.Parameters["@Ville"].Value = client.City;
                command.Parameters["@Province"].Value = client.State;

                ClientsDbFnc.isOpen = true;
                ClientsDbFnc.Connection.Open();
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Debug.Print(ex.Message);
                MessageBox.Show("Impossible de modifier le client.", "Une erreur est survenue...", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }
            finally
            {
                ClientsDbFnc.Connection.Close();

            }
            return true;
        }

    }
}
