﻿using MultiLocationsManager.Classes.Data;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using MultiLocationsManager.Classes.BAL;
using System.Diagnostics;

namespace MultiLocationsManager.Classes.DATA.SQL
{
    internal static class RentalDbFnc
    {
        private static SqlConnection Connection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
        internal static bool isOpen = false;


        internal static void Load(Rentals Rentals)
        {
            try
            {
                string command;

                command = $"SELECT [L].[ID] As [LocationID]" +
                          $"    ,[L].[DateDébutLocation]" +
                          $"    ,[L].[DatePremierPaeiement]" +
                          $"    ,[L].[MontantPremierPaiement]" +
                          $"    ,[L].[KilométrageDébutLocation]" +
                          $"    ,[L].[KilométrageFinLocation]" +
                          $"    ,[C].[ID] AS [ClientID]" +
                          $"    ,[C].[NuméroTéléphone]" +
                          $"    ,[C].[Prénom]" +
                          $"    ,[C].[Nom]" +
                          $"    ,[C].[Address]" +
                          $"    ,[C].[CodePostal]" +
                          $"    ,[C].[Ville]" +
                          $"    ,[C].[Province]" +
                          $"    ,[L].[Terms_LocationID] AS [Terms_LocationID]" +
                          $"    ,[TL].[KiloMax]" +
                          $"    ,[TL].[NombreAnnées]" +
                          $"    ,[TL].[NombrePaimentMensuel]" +
                          $"    ,[TL].[SurPrimeKilomètrage]" +
                          $"    ,[L].[VéhiculesNIV]" +
                          $"    ,[V].[Année]" +
                          $"    ,[V].[Valeur]" +
                          $"    ,[Co].[ID] AS [CouleurID]" +
                          $"    ,[Co].[Couleur]" +
                          $"    ,[T].[ID] AS [TypeID]" +
                          $"    ,[T].[Type]" +
                          $"    ,[M].[ID] AS [ModèleID]" +
                          $"    ,[M].[Modèle]" +
                          $"    ,[Tr].[ID] AS [TransmissionID]" +
                          $"    ,[Tr].[Transmission]" +
                          $"    ,[O].[ID] AS [OptionID]" +
                          $"    ,[O].[Option]" +
                          $"  FROM [MultiLocations6639].[dbo].[Locations] AS [L]" +
                          $"    Inner Join [Clients] as C on [ClientID] = [C].[ID]" +
                          $"    Inner Join [Termes_Locations] AS TL on [TL].ID = [L].Terms_LocationID" +
                          $"    Inner Join [Véhicules] AS V on [V].[NIV] = [L].[VéhiculesNIV]" +
                          $"    Inner Join [Couleurs] AS [Co] on [V].Couleur = [Co].ID" +
                          $"    Inner Join [Types] AS [T] ON [T].[ID] = [V].[Type]" +
                          $"    Inner Join [Modèles] AS [M] ON [M].[ID] = [V].[Modèle]" +
                          $"    Inner Join [Transmissions] AS [Tr] ON [Tr].[ID] = [V].[Transmission]" +
                          $"    Inner Join [Options] AS [O] ON [O].[ID] = [V].[Options];";

                SqlCommand sqlCommand = new SqlCommand(command, RentalDbFnc.Connection);

                RentalDbFnc.isOpen = true;
                RentalDbFnc.Connection.Open();
                SqlDataReader sqlReader = sqlCommand.ExecuteReader();
                if (sqlReader.HasRows)
                {
                    while (sqlReader.Read())
                    {
                        Rentals.Add(sqlReader);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                RentalDbFnc.isOpen = false;
                RentalDbFnc.Connection.Close();
            }
        }


        internal static bool Delete(Rental rental)
        {
            try
            {
                SqlCommand command = new SqlCommand($"DELETE FROM [MultiLocations6639].[dbo].[Paiements]" +
                                                    $"    WHERE [LocationID] = @ID;" +
                                                    $"DELETE FROM [MultiLocations6639].[dbo].[Audit]" +
                                                    $"    WHERE [LocationID] = @ID;" +
                                                    $"DELETE " +
                                                    $"  FROM [MultiLocations6639].[dbo].[Locations]" +
                                                    $"    WHERE [Id]=@ID;", RentalDbFnc.Connection);

                command.Parameters.Add("@ID", System.Data.SqlDbType.Int);
                command.Parameters["@ID"].Value = rental.Id;
                RentalDbFnc.isOpen = true;
                RentalDbFnc.Connection.Open();
                command.ExecuteNonQuery();
            } 
            catch (Exception ex)
            {
                Debug.Print(ex.Message);
                MessageBox.Show("Impossible de supprimer la location.", "Une erreur est survenue...", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }
            finally
            {
                RentalDbFnc.Connection.Close();
            }
            return true;
        }


        internal static bool Insert(Rental rental)
        {
            try
            {
                if (rental.Terms.Id == -1)
                {
                    RentalTermsDbFnc.InsertWithIDOutput(rental.Terms);
                }

                SqlCommand command = new SqlCommand($"INSERT INTO [MultiLocations6639].[dbo].[Locations] ( " +
                                                    $"   [DateDébutLocation]" +
                                                    $"  ,[DatePremierPaeiement]" +
                                                    $"  ,[MontantPremierPaiement]" +
                                                    $"  ,[ClientID]" +
                                                    $"  ,[Terms_LocationID]" +
                                                    $"  ,[VéhiculesNIV]" +
                                                    $"  ,[KilométrageDébutLocation]" +
                                                    $"  ,[KilométrageFinLocation]" +
                                                    $") VALUES ( " +
                                                    $"   @DateDebutLocation" +
                                                    $"  ,@DatePremierPaiement" +
                                                    $"  ,@MontantPremierPaiement" +
                                                    $"  ,@ClientID" +
                                                    $"  ,@TermsLocationID" +
                                                    $"  ,@VehiculesNIV" +
                                                    $"  ,@KilometrageDebutLocation" +
                                                    $"  ,@KilometrageFinLocation );", RentalDbFnc.Connection);

                command.Parameters.Add("@ID", System.Data.SqlDbType.BigInt);
                command.Parameters.Add("@DateDebutLocation", System.Data.SqlDbType.DateTime);
                command.Parameters.Add("@DatePremierPaiement", System.Data.SqlDbType.DateTime);
                command.Parameters.Add("@MontantPremierPaiement", System.Data.SqlDbType.Money);
                command.Parameters.Add("@ClientID", System.Data.SqlDbType.BigInt);
                command.Parameters.Add("@TermsLocationID", System.Data.SqlDbType.BigInt);
                command.Parameters.Add("@VehiculesNIV", System.Data.SqlDbType.VarChar);
                command.Parameters.Add("@KilometrageDebutLocation", System.Data.SqlDbType.BigInt);
                command.Parameters.Add("@KilometrageFinLocation", System.Data.SqlDbType.BigInt);

                command.Parameters["@ID"].Value = rental.Id;
                command.Parameters["@DateDebutLocation"].Value = rental.LocationStartDate;
                command.Parameters["@DatePremierPaiement"].Value = rental.FirstPaymentDate;
                command.Parameters["@MontantPremierPaiement"].Value = rental.FirstPaymentAmount;
                command.Parameters["@ClientID"].Value = rental.Client?.ID;
                command.Parameters["@TermsLocationID"].Value = rental.Terms?.Id;
                command.Parameters["@VehiculesNIV"].Value = rental.Vehicle?.Niv;
                command.Parameters["@KilometrageDebutLocation"].Value = rental.MileageAtStart;
                command.Parameters["@KilometrageFinLocation"].Value = rental.MileageAtEnd;
                RentalDbFnc.isOpen = true;
                RentalDbFnc.Connection.Open();
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Debug.Print(ex.Message);
                MessageBox.Show("Impossible de créer la location.", "Une erreur est survenue...", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }
            finally
            {
                RentalDbFnc.Connection.Close();

            }
            return true;

        }

        internal static bool Edit(Rental rental)
        {
            try
            {
                string commandText = "";
                // On insère un Termes_Locations si celui-ci n'existe pas.
                if (rental.Terms?.Id == -1)
                {
                    RentalTermsDbFnc.InsertWithIDOutput(rental.Terms);
                }

                commandText += $"UPDATE [MultiLocations6639].[dbo].[Locations] SET" +
                               $"  [DateDébutLocation] = @DateDebutLocation" +
                               $"  ,[DatePremierPaeiement] = @DatePremierPaiement" +
                               $"  ,[MontantPremierPaiement] = @MontantPremierPaiement" +
                               $"  ,[ClientID] = @ClientID" +
                               $"  ,[Terms_LocationID] = @TermsLocationID" +
                               $"  ,[VéhiculesNIV] = @VehiculesNIV" +
                               $"  ,[KilométrageDébutLocation] = @KilometrageDebutLocation" +
                               $"  ,[KilométrageFinLocation] = @KilometrageFinLocation" +
                               $"    WHERE [Id]=@ID;";

                SqlCommand command = new SqlCommand(commandText, RentalDbFnc.Connection);

                command.Parameters.Add("@ID", System.Data.SqlDbType.BigInt);
                command.Parameters.Add("@DateDebutLocation", System.Data.SqlDbType.DateTime);
                command.Parameters.Add("@DatePremierPaiement", System.Data.SqlDbType.DateTime);
                command.Parameters.Add("@MontantPremierPaiement", System.Data.SqlDbType.Money);
                command.Parameters.Add("@ClientID", System.Data.SqlDbType.BigInt);
                command.Parameters.Add("@TermsLocationID", System.Data.SqlDbType.BigInt);
                command.Parameters.Add("@VehiculesNIV", System.Data.SqlDbType.VarChar);
                command.Parameters.Add("@KilometrageDebutLocation", System.Data.SqlDbType.BigInt);
                command.Parameters.Add("@KilometrageFinLocation", System.Data.SqlDbType.BigInt);

                command.Parameters["@ID"].Value = rental.Id;
                command.Parameters["@DateDebutLocation"].Value = rental.LocationStartDate;
                command.Parameters["@DatePremierPaiement"].Value = rental.FirstPaymentDate;
                command.Parameters["@MontantPremierPaiement"].Value = rental.FirstPaymentAmount;
                command.Parameters["@ClientID"].Value = rental.Client?.ID;
                command.Parameters["@TermsLocationID"].Value = rental.Terms?.Id;
                command.Parameters["@VehiculesNIV"].Value = rental.Vehicle?.Niv;
                command.Parameters["@KilometrageDebutLocation"].Value = rental.MileageAtStart;
                command.Parameters["@KilometrageFinLocation"].Value = rental.MileageAtEnd;
                RentalDbFnc.isOpen = true;
                RentalDbFnc.Connection.Open();
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Debug.Print(ex.Message);
                MessageBox.Show("Impossible de modifier la location.", "Une erreur est survenue...", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }
            finally
            {
                RentalDbFnc.Connection.Close();

            }
            return true;
        }
    }
}
