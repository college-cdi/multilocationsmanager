﻿using MultiLocationsManager.Forms;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MultiLocationsManager.UserControls
{
    /// <summary>
    /// Logique d'interaction pour ToolBar.xaml
    /// </summary>
    public partial class ToolBar : UserControl
    {
        Window ParentWindow;
        #region "Constructor/Destructor"

        public ToolBar()
        {
            InitializeComponent();
        }

        #endregion


        #region "Form Events"

        public void ToolBar_Loaded(object sender, RoutedEventArgs e)
        {
            this.ParentWindow = Window.GetWindow(this);
        }


        /// <summary>
        /// 
        /// </summary>
        public void MnuAddUser_Click(object sender, RoutedEventArgs e)
        {
            frmRegisterUser registerUser = new frmRegisterUser();
            registerUser.ShowDialog();
        }


        /// <summary>
        /// 
        /// </summary>
        public void MnuAddRentals_Click(object sender, RoutedEventArgs e)
        {
            this.CloseParentWindow(new Forms.frmEditRental());
        }


        /// <summary>
        /// Gère 
        /// </summary>
        private void MnuShowRentals_Click(object sender, RoutedEventArgs e)
        {
            this.CloseParentWindow(new Forms.frmShowRental());
        }


        /// <summary>
        /// 
        /// </summary>
        public void MnuAddClients_Click(object sender, RoutedEventArgs e)
        {
            this.CloseParentWindow(new Forms.frmEditClient());
        }


        /// <summary>
        /// Gère 
        /// </summary>
        private void MnuShowClients_Click(object sender, RoutedEventArgs e)
        {
            this.CloseParentWindow(new Forms.frmShowClient());
        }


        private void btnLogout_Click(object sender, RoutedEventArgs e)
        {
            this.CloseParentWindow(new Forms.frmLoggin());
        }

        #endregion


        #region "Private Function"

        /// <summary>
        /// Ferme la fenêtre parente.
        /// </summary>
        private void CloseParentWindow(Window ParentWindow)
        {
            if (ParentWindow.Title != this.ParentWindow.Title)
            {
                ParentWindow.Show();
                this.ParentWindow.Close();
            }
        }

        #endregion
        
    }
}
