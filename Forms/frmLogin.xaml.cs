﻿using MultiLocationsManager.Classes.BAL;
using MultiLocationsManager.Classes.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MultiLocationsManager.Forms
{
    /// <summary>
    /// Logique d'interaction pour frmLogin.xaml
    /// </summary>
    public partial class frmLoggin : Window
    {
        private ActiveUser currentUser = new ActiveUser();

#region "Constructor/Destructor"

        /// <summary>
        /// Initialise le component.
        /// </summary>
        public frmLoggin()
        {
            InitializeComponent();
            txtUsername.Focus();
        }

        #endregion


        #region "Form events"

        private void login_Loaded(object sender, RoutedEventArgs e)
        {
            if (!AuthenticationDbFnc.Load())
            {
                // @TODO new form to authenticate users
                frmRegisterUser frmRegisterUser = new frmRegisterUser();
                frmRegisterUser.Show();
            }
        }


        /// <summary>
        /// 
        /// </summary>
        private void txtUsername_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            if (txtPassword.Password != null)
            {
                btnOk.IsEnabled = true;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        private void txtPassword_PasswordChanged(object sender, RoutedEventArgs e)
        {
            if (txtUsername.Text != null)
            {
                btnOk.IsEnabled = true;
            }
        }


        /// <summary>
        /// Connecte.
        /// </summary>
        private void btnOk_Click(object sender, EventArgs e)
        {
            this.currentUser.SetCredentials(txtUsername.Text, txtPassword.Password);
            if (MultiLocationsManager.Classes.Data.AuthenticationDbFnc.Authenticate(currentUser))
            {
                var RentalForm = new frmShowRental();
                RentalForm.Show();
                this.Close();
            }
            else
            {
                MessageBox.Show("Nom d'utilisateur ou mot de passe invalide."); // Or Label?
                txtUsername.Text = "";
                txtPassword.Password = "";
                txtUsername.Focus();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }


        /// <summary>
        /// Évenement lors de la fermeture de la page "Loggin".
        /// </summary>
        private void frmLoggin_Closing(object sender, CancelEventArgs e)
        {
            if (!this.currentUser.isLoggedIn)
            {
                // On s'assure que c'est bien l'intention de l'utilisateur de quitter l'application
                if (MessageBox.Show("Désirez-vous réellement quitter cette application ?", "Attention !", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                // Si tel est le cas, on met fin à l'application.
                {
                    Application.Current.Shutdown();
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }

        #endregion

    }
}
