﻿using MultiLocationsManager.Classes.BAL;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MultiLocationsManager.Forms
{
    /// <summary>
    /// Logique d'interaction pour frmShowLocations.xaml
    /// </summary>
    public partial class frmShowClient : Window
    {

        #region "Private Properties"

        private Clients Clients = new Clients();

        #endregion


        #region "Constructor"

        internal frmShowClient()
        {
            InitializeComponent();
            this.DataContext = this.Clients.Items;
            this.dgvClients.DataContext = this.Clients.Items;
            this.Clients.Load();
            this.btnShowDetails.IsEnabled = false;
            this.btnEditClient.IsEnabled = false;
            this.btnDeleteClient.IsEnabled = false;
        }

        #endregion


        #region "Form Events"

        private void frmShowClient_Loaded(object sender, RoutedEventArgs e)
        {
            this.dgvClients.ItemsSource = this.Clients.Items;
        }


        private void DataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.dgvClients.SelectedItems == null) return;

            if (this.dgvClients.SelectedItems.Count >= 0)
            {
                this.btnShowDetails.IsEnabled = this.dgvClients.SelectedItems.Count < 2;
                this.btnEditClient.IsEnabled = this.dgvClients.SelectedItems.Count < 2;
                this.btnDeleteClient.IsEnabled = true;
            } else
            {
                this.btnShowDetails.IsEnabled = false;
                this.btnEditClient.IsEnabled = false;
                this.btnDeleteClient.IsEnabled = false;
            }

            List<Client> Items = this.dgvClients.SelectedItems.Cast<Client>().ToList();

            foreach (var Item in this.Clients.Items)
            {
                if (this.dgvClients.SelectedItems.Contains(Item)) {
                    Item.isSelected = true;
                } else if (Item.isSelected == true)
                {
                    Item.isSelected = false;
                }
            }
            this.dgvClients.Items.Refresh();
        }


        private void btnShowDetails_Click(object sender, RoutedEventArgs e)
        {
            if (this.dgvClients.SelectedItems.Count != 1) return;

            Client Item = (Client) this.dgvClients.SelectedItem;

            if (this.Clients.Contains(Item))
            {
                /*frmEditClient showClient = new frmEditClient(Item);
                showClient.ShowDialog();
                this.Close();*/
            }
 
        }


        private void btnAddClient_Click(object sender, RoutedEventArgs e)
        {
            frmEditClient AddClient = new frmEditClient();
            AddClient.Show();
            this.Close();
        }


        private void btnEditClient_Click(object sender, RoutedEventArgs e)
        {
            if (this.dgvClients.SelectedItems.Count != 1) return;

            Client Item = (Client)this.dgvClients.SelectedItem;

            frmEditClient EditClient = new frmEditClient(Item);
            EditClient.Show();
            this.Close();
        }


        private void btnDeleteClient_Click(object sender, RoutedEventArgs e)
        {
            List<Client> Items = this.dgvClients.SelectedItems.Cast<Client>().ToList();
            List<Client> deletedItems = new List<Client>();
            foreach (var Item in this.Clients.Items)
            {
                if (this.dgvClients.SelectedItems.Contains(Item))
                {
                    if (this.Clients.Delete(Item)) 
                    {
                        deletedItems.Add(Item);
                    }
                }
            }
            foreach (Client deletedItem in deletedItems)
            {
                this.Clients.Items.Remove(deletedItem);
            }
            this.dgvClients.Items.Refresh();
        }

        
        private void btnFilter_Click(object sender, RoutedEventArgs e)
        {
            switch (this.cmbFilter.SelectedIndex)
            {
                case 0: // Prénom client
                    this.dgvClients.ItemsSource = this.Clients.Items.Where(x => x.FirstName.ToLower().Contains(this.txtFilter.Text.ToLower()));
                    this.dgvClients.Items.Refresh();
                    break;
                case 1: // Nom client
                    this.dgvClients.ItemsSource = this.Clients.Items.Where(x => x.LastName.ToLower().Contains(this.txtFilter.Text.ToLower()));
                    this.dgvClients.Items.Refresh();
                    break;
                case 2: // phone
                    this.dgvClients.ItemsSource = this.Clients.Items.Where(x => x.PhoneNumber.Contains(this.txtFilter.Text));
                    this.dgvClients.Items.Refresh();
                    break;
                case 3: // nom complet
                    this.dgvClients.ItemsSource = this.Clients.Items.Where(x => x.FullName.ToLower().Contains(this.txtFilter.Text.ToLower()));
                    this.dgvClients.Items.Refresh();
                    break;
                case 4: // Ville
                    this.dgvClients.ItemsSource = this.Clients.Items.Where(x => x.City.ToLower().Contains(this.txtFilter.Text.ToLower()));
                    this.dgvClients.Items.Refresh();
                    break;
                case 5: // Code Postal
                    this.dgvClients.ItemsSource = this.Clients.Items.Where(x => x.PostalCode.ToLower().Contains(this.txtFilter.Text.ToLower()));
                    this.dgvClients.Items.Refresh();
                    break;
                case 6: // Province
                    this.dgvClients.ItemsSource = this.Clients.Items.Where(x => x.State.ToLower().Contains(this.txtFilter.Text.ToLower()));
                    this.dgvClients.Items.Refresh();
                    break;
                default:
                    this.dgvClients.ItemsSource = this.Clients.Items;
                    return;
            }
        }


        private void btnFilterCancel_Click(object sender, RoutedEventArgs e)
        {
            this.txtFilter.Text = "";
            this.dgvClients.ItemsSource = this.Clients.Items;
            this.dgvClients.Items.Refresh();
        }

        #endregion 
    }
}
