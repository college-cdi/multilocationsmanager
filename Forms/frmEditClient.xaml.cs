﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MultiLocationsManager.Classes.BAL;
using System.Text.RegularExpressions;
using System.Configuration;
using System.Runtime.CompilerServices;
using System.Diagnostics;
using System.ComponentModel;

namespace MultiLocationsManager.Forms
{
    /// <summary>
    /// Logique d'interaction pour frmEditClient.xaml
    /// </summary>
    public partial class frmEditClient : Window
    {
        #region "Private Variables"

        private Client Client;
        private enuMode Mode;
        private bool WindowIsLoaded = false;
        private List<States> cmbListStates = new List<States>();

        private enum enuMode
        {
            Edit,
            Create
        }

        #endregion

        #region "Constructor"

        internal frmEditClient(Client Client)
        {
            this.Mode = enuMode.Edit;
            this.Client = Client;
            InitializeComponent();
        }


        internal frmEditClient()
        {
            this.Mode = enuMode.Create;
            this.Client = new Client();
            InitializeComponent();
        }

        #endregion


        #region "Private Form Events"

        private void frmEditClient_Loaded(object sender, RoutedEventArgs e)
        {
            this.LoadStates();

            if (this.Mode == enuMode.Edit)
            {
                this.Title = "Modifier client";
                this.btnAction.Content = "Modifier";
                this.SetValues();
            }
            else
            {
                this.Title = "Ajouter nouveau client";
                this.btnAction.Content = "Créer";
                this.ClearValues();
            }

            this.WindowIsLoaded = true;
        }


        private void btnAction_Click(object sender, RoutedEventArgs e)
        {

            if (String.IsNullOrEmpty(this.txtFirstName.Text) ||
                String.IsNullOrEmpty(this.txtLastName.Text) ||
                String.IsNullOrEmpty(this.txtPhoneNumber.Text) ||
                String.IsNullOrEmpty(this.txtPostalCode.Text) ||
                String.IsNullOrEmpty(this.txtCity.Text) ||
                String.IsNullOrEmpty(this.txtAddress.Text) ||
                this.cmbState.SelectedItem is null)  return;

            if (this.Client is null) this.Client = new Client();

            this.Client.SetFirstName(this.txtFirstName.Text)
                       .SetLastName(this.txtLastName.Text)
                       .SetPhoneNumber(this.txtPhoneNumber.Text)
                       .SetPostalCode(this.txtPostalCode.Text)
                       .SetCity(this.txtCity.Text)
                       .SetState(this.cmbState.SelectedItem.ToString())
                       .SetAddress(this.txtAddress.Text);

            if (this.Mode == enuMode.Edit) this.Client.Edit();
            else this.Client.Insert();

            frmShowClient frmShowClient = new frmShowClient();
            frmShowClient.Show();
            this.Close();
        }


        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            if (this.Mode == enuMode.Create) this.ClearValues();
            else this.SetValues();
        }


        private void PostalCodeValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            //@TODO Regex
        }


        private void PhoneNumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        #endregion


        #region "Private Function"

        private void LoadStates()
        {
            this.cmbListStates.Add(new States("Qc", "Quebec"));
            this.cmbListStates.Add(new States("On", "Ontario"));
            this.cmbListStates.Add(new States("Ns", "Nova Scotia"));
            this.cmbListStates.Add(new States("Nb", "Nouveau Brunswick"));
            this.cmbListStates.Add(new States("Mb", "Manitoba"));
            this.cmbListStates.Add(new States("Pe", "Ïle du prince Édouard"));
            this.cmbListStates.Add(new States("Sk", "Saskatchewan"));
            this.cmbListStates.Add(new States("Ab", "Alberta"));
            this.cmbListStates.Add(new States("Nl", "Terre-Neuve-et-Labrador"));
            this.cmbListStates.Add(new States("Bc", "Colombie-Britannique"));

            this.cmbState.ItemsSource = this.cmbListStates;
            this.cmbState.SelectedValuePath = "Text";
            this.cmbState.DisplayMemberPath = "Value";
        }


        private void SetValues()
        {
            if (this.Client == null) return;
            this.txtFirstName.Text = this.Client.FirstName;
            this.txtLastName.Text = this.Client.LastName;
            this.txtPhoneNumber.Text = this.Client.PhoneNumber;
            this.txtCity.Text = this.Client.City;
            this.txtPostalCode.Text = this.Client.PostalCode;
            this.txtAddress.Text = this.Client.Address;
            var state = this.cmbListStates.Find(x => x.Text == this.Client.State);
            this.cmbState.SelectedValue = state;
        }


        private void ClearValues()
        {
            // Hide Mileage at End of rent as we are creating a new one
            this.txtFirstName.Text = "";
            this.txtLastName.Text = "";
            this.txtPhoneNumber.Text = "";
            this.txtCity.Text = "";
            this.txtAddress.Text = "";
            this.txtPostalCode.Text = "";
            this.cmbState.SelectedIndex = 0;
        }

        #endregion

    }
}
