﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MultiLocationsManager.Classes.BAL;
using System.Text.RegularExpressions;
using System.Configuration;
using System.Runtime.CompilerServices;
using System.Diagnostics;

namespace MultiLocationsManager.Forms
{
    /// <summary>
    /// Logique d'interaction pour frmEditRental.xaml
    /// </summary>
    public partial class frmEditRental : Window
    {
        #region "Private Variables"

        private Rental Rental;
        private Vehicles? Vehicles = new Vehicles();
        private RentalTerms? RentalTerms = new RentalTerms();
        private Clients? Clients = new Clients();
        private enuMode Mode;
        private bool WindowIsLoaded = false;

        private enum enuMode
        {
            Edit,
            Create
        }

        #endregion

        #region "Constructor"

        internal frmEditRental(Rental rental)
        {
            this.Mode = enuMode.Edit;
            this.Rental = rental;
            InitializeComponent();

            this.cmbVehicules_Load();
            this.cmbClients_Load();
            this.cmbRentalTerms_Load();
        }


        internal frmEditRental()
        {
            this.Mode = enuMode.Create;
            this.Rental = new Rental();
            InitializeComponent();

            this.cmbVehicules_Load();
            this.cmbClients_Load();
            this.cmbRentalTerms_Load();
        }

        #endregion


        #region "Private Form Events"

        private void frmEditRental_Loaded(object sender, RoutedEventArgs e)
        {
            if (this.Mode == enuMode.Edit)
            {
                this.Title = "Modifier Location";
                this.btnAction.Content = "Modifier";
                this.SetValues();
            }
            else
            {
                this.Title = "Créer Location";
                this.btnAction.Content = "Créer";
                this.ClearValues();
            }
            this.WindowIsLoaded = true;
        }


        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }


        private void DecimalValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("/^\\d*\\.?\\d*$/");
            e.Handled = regex.IsMatch(e.Text);
        }

        
        private void txtBoxAgeValidation(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^1-4]");
            e.Handled = regex.IsMatch(e.Text) || !(System.Convert.ToInt64(this.txtRentalTerms_Age.Text + e.Text) < 5);
            this.txtRentalTermsNumbersOfMonthlyPayments.Text = (int.Parse(e.Text) * 12).ToString();
        }


        private void cmbVehicules_Load()
        {
            this.Vehicles?.Load();
            this.cmbVehicle.ItemsSource = this.Vehicles?.Items;
            this.cmbVehicle.SelectedValuePath = "Niv";
            this.cmbVehicle.DisplayMemberPath = "_Model";
        }


        private void cmbClients_Load()
        {
            this.Clients?.Load();
            this.cmbClient.ItemsSource = this.Clients?.Items;
            this.cmbClient.SelectedValuePath = "ID";
            this.cmbClient.DisplayMemberPath = "FullName";
        }


        private void cmbRentalTerms_Load()
        {
            this.RentalTerms?.Load();
            this.cmbRentalTerms.ItemsSource = this.RentalTerms?.Items;
            this.cmbRentalTerms.SelectedValuePath = "Id";
            this.cmbRentalTerms.DisplayMemberPath = "Id";
        }


        private void HandleRentalTermsInputs(object sender, SelectionChangedEventArgs e)
        {
            if (!this.WindowIsLoaded) return;
            if (this.checkRentalTerms.IsChecked == null) this.checkRentalTerms.IsChecked = false;
            if (!(bool)this.checkRentalTerms.IsChecked) this.SetRentalTermsValues((RentalTerm)this.cmbRentalTerms.SelectedItem);
            else this.ClearRentalTermsValues();
        }


        private void HandleRentalTermsCheckBox(object sender, RoutedEventArgs e)
        {
            if (this.checkRentalTerms.IsChecked == null) this.checkRentalTerms.IsChecked = false;
            bool enable = (bool)this.checkRentalTerms.IsChecked;
            this.cmbRentalTerms.IsEnabled = !enable;
            this.txtRentalTerms_Age.IsEnabled = enable;
            this.txtRentalTerms_KiloMax.IsEnabled = enable;
            this.txtRentalTerms_OverMileagePrice.IsEnabled = enable;

            if (!enable) this.SetRentalTermsValues((RentalTerm)this.cmbRentalTerms.SelectedItem);
        }


        private void btnAction_Click(object sender, RoutedEventArgs e)
        {
            if (this.dteDateStartLocation.SelectedDate is null ||
                this.dteFirstPaymentDate.SelectedDate is null ||
                String.IsNullOrEmpty(this.txtFirstPaymentAmount.Text) ||
                String.IsNullOrEmpty(this.txtMileageEndLocation.Text) ||
                String.IsNullOrEmpty(this.txtMileageStartLocation.Text) ||
                this.cmbVehicle.SelectedItem is null ||
                this.cmbClient.SelectedItem is null ||
                this.AreRentalTermsFieldsReady())  return;

            if (!this.AreTextBoxesValid()) return;
            if (this.Rental is null) this.Rental = new Rental();

            this.Rental.SetFirstPaymentAmount(this.txtFirstPaymentAmount.Text)
                       .SetLocationStartDate((DateTime)this.dteDateStartLocation.SelectedDate)
                       .SetFirstPaymentDate((DateTime)this.dteFirstPaymentDate.SelectedDate)
                       .SetVehicle((Vehicle)this.cmbVehicle.SelectedItem)
                       .SetMileageAtStart(this.txtMileageStartLocation.Text)
                       .SetClient((Client)this.cmbClient.SelectedItem)
                       .SetMileageAtEnd(this.txtMileageEndLocation.Text);

            if (this.checkRentalTerms.IsChecked == null) this.checkRentalTerms.IsChecked = false;
            if (!(bool)this.checkRentalTerms.IsChecked) this.Rental.SetTerms((RentalTerm)this.cmbRentalTerms.SelectedItem);
            else
            {
                this.Rental.SetTerms(new RentalTerm());
                this.Rental.Terms?.SetAge(int.Parse(this.txtRentalTerms_Age.Text))
                    .SetMaxMileage(int.Parse(this.txtRentalTerms_KiloMax.Text))
                    .SetNumbersOfMonthlyPayments(this.Rental.Terms.Age * 12)
                    .SetOverMileagePrice(double.Parse(this.txtRentalTerms_OverMileagePrice.Text));
            }

            if (this.Mode == enuMode.Edit) this.Rental.Edit();
            else this.Rental.Insert();

            frmShowRental frmShowRental = new frmShowRental();
            frmShowRental.Show();
            this.Close();
        }


        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            if (this.Mode == enuMode.Create) this.ClearValues();
            else this.SetValues();
        }

        #endregion


        #region "Private Function"

        private void SetValues()
        {
            if (this.Rental == null) return;
            this.txtFirstPaymentAmount.Text = this.Rental.FirstPaymentAmount.ToString();
            this.txtMileageStartLocation.Text = this.Rental.MileageAtStart.ToString();
            this.txtMileageEndLocation.Text = this.Rental.MileageAtEnd.ToString();
            this.dteDateStartLocation.SelectedDate = this.Rental.LocationStartDate;
            this.dteFirstPaymentDate.SelectedDate = this.Rental.FirstPaymentDate;
            this.cmbClient.SelectedValue = this.Rental.Client?.ID;
            this.cmbVehicle.SelectedValue = this.Rental.Vehicle?.Niv;

            if (this.Rental.Terms == null) return;
            this.cmbRentalTerms.SelectedValue = this.Rental.Terms.Id;
            this.SetRentalTermsValues(this.Rental.Terms);
        }


        private void SetRentalTermsValues(RentalTerm selectedItem)
        {
            this.txtRentalTermsNumbersOfMonthlyPayments.Text = selectedItem.NumbersOfMonthlyPayments.ToString();
            this.txtRentalTerms_Age.Text = selectedItem.Age.ToString();
            this.txtRentalTerms_KiloMax.Text = selectedItem.MaxMileage.ToString();
            this.txtRentalTerms_OverMileagePrice.Text = selectedItem.OverMilagePrice.ToString();

        }


        private void ClearValues()
        {
            // Hide Mileage at End of rent as we are creating a new one
            this.txtMileageEndLocation.Visibility = Visibility.Collapsed;
            this.lblMileageEndLocation.Visibility = Visibility.Collapsed;

            this.txtFirstPaymentAmount.Text = "";
            this.txtMileageStartLocation.Text = "";
            this.txtMileageEndLocation.Text = "0";
            this.dteDateStartLocation.SelectedDate = DateTime.Today;
            this.dteFirstPaymentDate.SelectedDate = DateTime.Today;
            this.cmbClient.SelectedIndex = 0;
            this.cmbVehicle.SelectedIndex = 0;
            this.cmbRentalTerms.SelectedIndex = 0;
        }


        private void ClearRentalTermsValues()
        {
            this.txtRentalTermsNumbersOfMonthlyPayments.Text = "";
            this.txtRentalTerms_Age.Text = "";
            this.txtRentalTerms_KiloMax.Text = "";
            this.txtRentalTerms_OverMileagePrice.Text = "";
        }


        private bool AreRentalTermsFieldsReady()
        {
            if (this.checkRentalTerms.IsChecked == null) this.checkRentalTerms.IsChecked = false;
            return (!(bool)this.checkRentalTerms.IsChecked &&
                String.IsNullOrEmpty(this.txtRentalTerms_Age.Text) &&
                String.IsNullOrEmpty(this.txtRentalTerms_KiloMax.Text) &&
                String.IsNullOrEmpty(this.txtRentalTerms_OverMileagePrice.Text));
        }


        private bool AreTextBoxesValid()
        {
            if (!double.TryParse(this.txtFirstPaymentAmount.Text, out double _))
            {
                MessageBox.Show("Impossible de modifier/créer l'élément : Le premier paiement n'est pas un nombre.", "NaN", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }
            else if (!int.TryParse(this.txtMileageStartLocation.Text, out int _))
            {
                MessageBox.Show("Impossible de modifier/créer l'élément: le Kilométrage de début de location n'est pas un nombre.", "NaN", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }
            else if (!int.TryParse(this.txtMileageEndLocation.Text, out int _))
            {
                MessageBox.Show("Impossible de modifier/créer l'élément: le kilométrage de fin de location n'est pas un nombre.", "NaN", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }
            else if (!int.TryParse(this.txtRentalTerms_Age.Text, out int _))
            {
                MessageBox.Show("Impossible de modifier/créer l'élément: le nombre d'année du terme de location n'est pas un nombre.", "NaN", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }
            else if (!int.TryParse(this.txtRentalTerms_KiloMax.Text, out int _))
            {
                MessageBox.Show("Impossible de modifier/créer l'élément: le kilométrage maximal de la location n'est pas un nombre.", "NaN", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }
            else if (!int.TryParse(txtRentalTermsNumbersOfMonthlyPayments.Text, out int _))
            {
                MessageBox.Show("Impossible de modifier/créer l'élément: le nombre de paiement mensuel de la location n'est pas un nombre.", "NaN", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }
            else if (!double.TryParse(this.txtRentalTerms_OverMileagePrice.Text, out double _))
            {
                MessageBox.Show("Impossible de modifier/créer l'élément: la surprime de kilométrage de la location n'est pas un nombre.", "NaN", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }

            return true;
        }

        #endregion

    }
}
