﻿using MultiLocationsManager.Classes.BAL;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MultiLocationsManager.Forms
{
    /// <summary>
    /// Logique d'interaction pour frmShowLocations.xaml
    /// </summary>
    public partial class frmShowRental : Window
    {

        #region "Private Properties"

        private Rentals Rentals = new Rentals();

        #endregion


        #region "Constructor"

        internal frmShowRental()
        {
            InitializeComponent();
            this.DataContext = this.Rentals.Items;
            this.dgvRentals.DataContext = this.Rentals.Items;
            this.Rentals.Load();
            this.btnShowDetails.IsEnabled = false;
            this.btnEditRental.IsEnabled = false;
            this.btnDeleteRental.IsEnabled = false;
        }

        #endregion


        #region "Form Events"

        private void frmShowRental_Loaded(object sender, RoutedEventArgs e)
        {
            this.dgvRentals.ItemsSource = this.Rentals.Items;
        }


        private void DataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.dgvRentals.SelectedItems == null) return;

            if (this.dgvRentals.SelectedItems.Count >= 0)
            {
                this.btnShowDetails.IsEnabled = this.dgvRentals.SelectedItems.Count < 2;
                this.btnEditRental.IsEnabled = this.dgvRentals.SelectedItems.Count < 2;
                this.btnDeleteRental.IsEnabled = true;
            } else
            {
                this.btnShowDetails.IsEnabled = false;
                this.btnEditRental.IsEnabled = false;
                this.btnDeleteRental.IsEnabled = false;
            }

            List<Rental> Items = this.dgvRentals.SelectedItems.Cast<Rental>().ToList();

            foreach (var Item in this.Rentals.Items)
            {
                if (this.dgvRentals.SelectedItems.Contains(Item)) {
                    Item.isSelected = true;
                } else if (Item.isSelected == true)
                {
                    Item.isSelected = false;
                }
            }
            this.dgvRentals.Items.Refresh();
        }


        private void btnShowDetails_Click(object sender, RoutedEventArgs e)
        {
            if (this.dgvRentals.SelectedItems.Count != 1) return;

            Rental Item = (Rental) this.dgvRentals.SelectedItem;

            if (this.Rentals.Contains(Item))
            {
                /*frmEditRental showRental = new frmEditRental(Item);
                showRental.ShowDialog();
                this.Close();*/
            }
 
        }


        private void btnAddRental_Click(object sender, RoutedEventArgs e)
        {
            frmEditRental AddRental = new frmEditRental();
            AddRental.Show();
            this.Close();
        }


        private void btnEditRental_Click(object sender, RoutedEventArgs e)
        {
            if (this.dgvRentals.SelectedItems.Count != 1) return;

            Rental Item = (Rental)this.dgvRentals.SelectedItem;

            frmEditRental EditRental = new frmEditRental(Item);
            EditRental.Show();
            this.Close();
        }


        private void btnDeleteRental_Click(object sender, RoutedEventArgs e)
        {
            List<Rental> Items = this.dgvRentals.SelectedItems.Cast<Rental>().ToList();
            List<Rental> deletedItems = new List<Rental>();
            foreach (var Item in this.Rentals.Items)
            {
                if (this.dgvRentals.SelectedItems.Contains(Item))
                {
                    if (this.Rentals.Delete(Item)) 
                    {
                        deletedItems.Add(Item);
                    }
                }
            }

            foreach (var deletedItem in deletedItems)
            {
                this.Rentals.Items.Remove(deletedItem);
            }
            this.dgvRentals.Items.Refresh();
        }

        
        private void btnFilter_Click(object sender, RoutedEventArgs e)
        {
            // @TODO Filter
            switch(this.cmbFilter.SelectedIndex)
            {
                case 0: // Prénom client
                    this.dgvRentals.ItemsSource = this.Rentals.Items.Where(x => x.Client.FirstName.ToLower().Contains(this.txtFilter.Text.ToLower()));
                    this.dgvRentals.Items.Refresh();
                    break;
                case 1: // Nom client
                    this.dgvRentals.ItemsSource = this.Rentals.Items.Where(x => x.Client.LastName.ToLower().Contains(this.txtFilter.Text.ToLower()));
                    this.dgvRentals.Items.Refresh();
                    break;
                case 2: // phone
                    this.dgvRentals.ItemsSource = this.Rentals.Items.Where(x => x.Client.PhoneNumber.Contains(this.txtFilter.Text));
                    this.dgvRentals.Items.Refresh();
                    break;

                case 3: // nom complet
                    this.dgvRentals.ItemsSource = this.Rentals.Items.Where(x => x.Client.FullName.ToLower().Contains(this.txtFilter.Text.ToLower()));
                    this.dgvRentals.Items.Refresh();
                    break;

                case 4: // niv
                    this.dgvRentals.ItemsSource = this.Rentals.Items.Where(x => x.Vehicle.Niv.ToLower().Contains(this.txtFilter.Text.ToLower()));
                    this.dgvRentals.Items.Refresh();
                    break;

                case 5: // modele
                    this.dgvRentals.ItemsSource = this.Rentals.Items.Where(x => x.Vehicle.Model.Name.ToLower().Contains(this.txtFilter.Text.ToLower()));
                    this.dgvRentals.Items.Refresh();
                    break;
                default:
                    this.dgvRentals.ItemsSource = this.Rentals.Items;
                    return;
            }
        }


        private void btnFilterCancel_Click(object sender, RoutedEventArgs e)
        {
            this.txtFilter.Text = "";
            this.dgvRentals.ItemsSource = this.Rentals.Items;
            this.dgvRentals.Items.Refresh();
        }

        #endregion 
    }
}
