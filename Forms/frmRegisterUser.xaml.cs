﻿using MultiLocationsManager.Classes.BAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MultiLocationsManager.Forms
{
    /// <summary>
    /// Logique d'interaction pour frmRegisterUser.xaml
    /// </summary>
    public partial class frmRegisterUser : Window
    {
        private ActiveUser User = new ActiveUser();
        public frmRegisterUser()
        {
            InitializeComponent();
            txtFirstName.Focus();
        }

        /// <summary>
        /// 
        /// </summary>
        private void txtPassword_PasswordChanged(object sender, RoutedEventArgs e)
        {
            this.lblError.Content = "";
            if (txtPassword.Password != txtConfirmPassword.Password)
            {
                this.lblError.Content = "Les mots de passes doivent être identiques.";
                return;
            }
        }


        /// <summary>
        /// Connecte.
        /// </summary>
        private void btnOk_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(this.txtFirstName.Text) ||
                String.IsNullOrEmpty(this.txtLastName.Text) ||
                String.IsNullOrEmpty(this.txtUsername.Text) ||
                String.IsNullOrEmpty(this.txtPassword.Password) ||
                String.IsNullOrEmpty(this.txtConfirmPassword.Password) ||
                this.txtPassword.Password != this.txtConfirmPassword.Password)
            {
                this.lblError.Content = "Veuillez remplir tous les champs avant d'enrigstrer l'utilisateur!";
                return;
            }

            this.User.SetFirstName(this.txtFirstName.Text)
                .SetLastName(this.txtLastName.Text)
                .SetUserName(this.txtUsername.Text)
                .SetPassword(this.txtPassword.Password)
                .Encrypt();
            
            if (this.User.Register())
            {
                MessageBox.Show("Utilisateur créer avec succès!");
                this.Close();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
